<?php
  if (!function_exists('dd')) {
    function dd($data)
    {
      dump($data);
      exit();
    }
  }

  if (!function_exists('asset_url')) {
    function asset_url()
    {
      return base_url().'assets/';
    }
  }

  if (!function_exists('page_url')) {
    function page_url()
    {
      return base_url().'//localhost/blogs/';
    }
  }
?>