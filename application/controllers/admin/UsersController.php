<?php
	class UsersController extends BaseController
	{
		public function construct()
		{
			parent::__construct();
      $this->load->helper(['common', 'autoload']);
		}

		public function index()
		{
      $data['content'] = 'admin/users/index';
      $this->load->view('admin/templates/content', $data);
		}

    public function create()
    {
      $data['content'] = 'admin/users/form';
      $this->load->view('admin/templates/content', $data);
    }
	}
?>