<?php
  class WelcomeController extends CI_Controller
  {
    public function __construct()
    {
      parent::__construct();
    }

    public function index()
    {
      $data['content'] = 'admin/welcome';
      $a = $this->load->view('admin/templates/content', $data);
    }
  }
?>