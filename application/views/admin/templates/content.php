<div class="wrapper">
  <?php $this->load->view('admin/templates/header'); ?>
  <?php $this->load->view('admin/templates/left_menu'); ?>
  <div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
      <?php $this->load->view('admin/templates/top'); ?>
    </nav>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <?php $this->load->view($content); ?>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/templates/footer'); ?>
</div>