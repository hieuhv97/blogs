<div class="col-md-12">
  <div class="card">
    <div class="header">
        <a href="//localhost/blogs/users/create" class="btn btn-primary btn-sm right">New users</a>
    </div>
    <div class="content table-responsive table-full-width">
      <table class="table table-hover table-striped">
        <thead>
          <tr><th>ID</th>
            <th>Name</th>
            <th>Salary</th>
            <th>Country</th>
            <th>City</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>6</td>
            <td>Mason Porter</td>
            <td>$78,615</td>
            <td>Chile</td>
            <td>Gloucester</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>