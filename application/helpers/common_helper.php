<?php
  if(!function_exists("template")) {
    function template($content = "welcome")
    {
      $this->load->view('templates/header');
      $this->load->view('templates/left_menu');
      $this->load->view('templates/$content');
      $this->load->view('templates/footer');
    }
  }

  if(!function_exists("getName")) {
    function getName()
    {
      base_url()
    }
  }
?>