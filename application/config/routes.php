<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['dashboard'] = 'admin/WelcomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['users']['GET'] = 'admin/UsersController';
$route['users/create']['GET'] = 'admin/UsersController/create';
